# Info

1. Java 11
2. Spring Data Rest (Hypermedia API)
3. Swagger UI http://localhost:8080/swagger-ui/#/

# DBMS Startup

1. Download latest version of DBMS https://www.enterprisedb.com/downloads/postgres-postgresql-downloads
2. Install it https://www.robinwieruch.de/postgres-sql-macos-setup
3. Start it `pg_ctl -D /usr/local/var/postgres start` Linux/MacOS 
4. With `psql` command create database/schema/user, grant privileges to user. https://www.google.com/search?q=create+schema+postgresql

