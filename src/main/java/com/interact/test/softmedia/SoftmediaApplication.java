package com.interact.test.softmedia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoftmediaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SoftmediaApplication.class, args);
	}
}
