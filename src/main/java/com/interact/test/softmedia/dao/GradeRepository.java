package com.interact.test.softmedia.dao;

import com.interact.test.softmedia.domain.Grade;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GradeRepository extends CrudRepository<Grade, Long> {

}