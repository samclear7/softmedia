package com.interact.test.softmedia.dao;

import com.interact.test.softmedia.domain.Student;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends CrudRepository<Student, Long> {

}