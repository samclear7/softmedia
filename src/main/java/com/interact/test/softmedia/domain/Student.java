package com.interact.test.softmedia.domain;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Entity
@Table
public class Student {

    @Id
    private Long id;

    @Column    
    @NotBlank
    @NotNull
    private String fullName;

    @Column
    @NotNull
    private LocalDate dateOfBirth;

    @OneToMany(mappedBy = "student")
    private List<Grade> grades;

}